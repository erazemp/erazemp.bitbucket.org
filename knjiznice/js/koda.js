
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var tabEhrId = {};

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  
  var sessionId = getSessionId();
  
  var ime,priimek,dob,telesnaTeza,sistolicniKrvniTlak,diastolicniKrvniTlak,ehrId;
  
  if (stPacienta == 1){
     ime = "Ankastazijanovanastazijanasta";
     priimek = "Svengali";
     dob = "1958-03-19";
     telesnaTeza = "60";
     sistolicniKrvniTlak = "111";
     diastolicniKrvniTlak = "95";
  }
  if (stPacienta == 2){
     ime = "Erki";
     priimek = "Berki";
     dob = "1985-09-11";
     telesnaTeza = "79";
     sistolicniKrvniTlak = "130";
     diastolicniKrvniTlak = "83";
  }
  if (stPacienta == 3){
     ime = "Erko";
     priimek = "Puška";
     dob = "2007-11-03";
     telesnaTeza = "80";
     sistolicniKrvniTlak = "115";
     diastolicniKrvniTlak = "87";
  }
  
  
  $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: dob,
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
	        $.ajax({
		    url: baseUrl + "/view/" + ehrId + "weight",
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	        
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	               $("#abc").append("<option value='"+ehrId+"'>"+ime+" "+priimek+" "+dob+"</option>");
	            },
	            error: function(err) {
	            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                 "label-danger fade-in'>Napaka '" +
                 JSON.parse(err.responseText).userMessage + "'!");
	            }
	      });
      }
	});
  
  

  // TODO: Potrebno implementirati
   //alert(tabEhrId[0]);
  //return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija


function resetirajFormo(){
   var ehrId = document.getElementById("izpisEhr").value;
   
   var sessionId = getSessionId();
   
   if (ehrId.length == 0){
      document.getElementById("izpisIme").value = "";
	   document.getElementById("izpisPriimek").value = "";
	   document.getElementById("izpisDatumRojstva").value = "";
	   document.getElementById("izpisStarost").value = "";
	   document.getElementById("izpisKg").value = "";
	   document.getElementById("izpisDiastolicni").value = "";
	   document.getElementById("izpisSistolicni").value = "";
	   return;
   }
   

   $.ajax({
		    url: baseUrl + "/view/" + ehrId + "/weight",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		       document.getElementById("izpisKg").value = res[0].weight;
		       document.getElementById("izpisSistolicni").value = res[0].weight;
		       document.getElementById("izpisDiastolicni").value = res[0].weight;
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
   
   $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				document.getElementById("izpisIme").value = party.firstNames;
				document.getElementById("izpisPriimek").value = party.lastNames;
				document.getElementById("izpisDatumRojstva").value = party.dateOfBirth;
				document.getElementById("izpisStarost").value = izracunStarosti(party.dateOfBirth);
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
}

function resetID(){
   var x = document.getElementById("abc").selectedIndex;
   var ehrId = document.getElementById("abc").options[x].value;
   document.getElementById("izpisEhr").value = ehrId;
   
}

function generirajPodatkeVse(){
   $("#abc").html("<option value=''></option>");
   document.getElementById("izpisEhr").value = "";
   document.getElementById("izpisIme").value = "";
	document.getElementById("izpisPriimek").value = "";
	document.getElementById("izpisDatumRojstva").value = "";
	document.getElementById("izpisStarost").value = "";
   generirajPodatke(1);
   generirajPodatke(2);
   generirajPodatke(3);
}


function izracunStarosti(dob) {
   var datum = dob.split("-");
   var leto = datum[0];
   var mesec = datum[1];
   var dan = datum[2];
   
   var danes = new Date();
   var starost = danes.getFullYear() - leto;
   if (danes.getMonth() < mesec || (danes.getMonth() == mesec && danes.getDate() < dan)) {
      starost--;
   }
   
   
   return starost;
   
   
}

function vpisBolnika(){
   
   
   var sessionId = getSessionId();
  
  var ime,priimek,dob,telesnaTeza,sistolicniKrvniTlak,diastolicniKrvniTlak,ehrId;
  
   ime = document.getElementById("vpisIme").value;
   priimek = document.getElementById("vpisPriimek").value;
   dob = document.getElementById("vpisDatumRojstva").value;
   telesnaTeza = document.getElementById("vpisKg").value;
   sistolicniKrvniTlak = document.getElementById("vpisSistlicni").value;
   diastolicniKrvniTlak = document.getElementById("vpisDiastolicni").value;
  
  document.getElementById("vpisKg").value = "";
  document.getElementById("vpisDiastolicni").value = "";
   document.getElementById("vpisSistlicni").value = "";
  document.getElementById("vpisIme").value = "";
  document.getElementById("vpisPriimek").value = "";
  document.getElementById("vpisDatumRojstva").value = "";
  
  $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: dob,
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	               $("#abc").append("<option value='"+ehrId+"'>"+ime+" "+priimek+" "+dob+"</option>");
	            },
	            error: function(err) {
	            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                 "label-danger fade-in'>Napaka '" +
                 JSON.parse(err.responseText).userMessage + "'!");
	            }
	      });
      }
	});
	
	
	var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		
		$.ajax({
		    url: baseUrl + "/view/" + ehrId + "/weight",
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
  
   
}


/* global $ $ajax*/